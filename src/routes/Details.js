import React, { useEffect, useState } from 'react'
import {useParams, useSearchParams } from 'react-router-dom';
import './Details.css'
function Details() {
    let {todoId} = useParams();
    let [searchParams, setSearchParams] = useSearchParams();
    console.log(searchParams.get('id'))
    let queryId = searchParams.get('id')
   let [todo, setToDo] = useState([]);
   
    useEffect(() => {
      fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
      .then(res => res.json())
      .then(res => setToDo(res))
  }, [todoId]);
   
   
    useEffect(() => {
      fetch(`https://jsonplaceholder.typicode.com/todos/${queryId}`)
      .then(res => res.json())
      .then(res => setToDo(res))
  }, [queryId]);
  return (
  
     <table id='toDoTable'>
        <thead id='toDoHeader'>
        <tr>
            <th>UserId</th>
            <th>Id</th>
            <th>Title</th>
            <th>Completed</th>
        </tr>
        </thead>
        <tbody>
          <tr>
            <td>{todo.userId}</td>
            <td>{todo.id}</td>
            <td>{todo.title}</td>
            <td>{String(todo.completed)}</td>
          </tr>
        </tbody>
      </table>
    
    
  )
}

export default Details