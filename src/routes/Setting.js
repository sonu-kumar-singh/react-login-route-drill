import React from 'react'
import { useState } from 'react';
import {ProfileContext} from '../Contexts/ProfileContextComponent'


function Setting() {
  const [inputs, setInputs] = useState({});
   
  return (
     <ProfileContext.Consumer>
      {(value) => {
         const {password, changeData} = value
         const handleChange = (event) => {
          const name = event.target.name;
          const value = event.target.value;
          setInputs(values => 
            ({...values, [name]: value}))
        }
         console.log(password,inputs.oldPassword)

         const handleSettingSubmit = (event) => {
          event.preventDefault();
          if(inputs.oldPassword === password){
            changeData(inputs.newPassword)
            console.log('in if')
          }
          else{
            console.log('data unmatched')
          }
         }

       return (
        <>
          <form onSubmit={handleSettingSubmit}>
                  <label>oldPassword<br/>
                  <input 
                    type="text" 
                    name="oldPassword" 
                    value={inputs.oldPassword || ""} 
                    onChange={handleChange}
                    placeholder='OldPassword...'
                    required
                  />
                  </label>
                  <label>Password<br/>
                    <input 
                      type="text" 
                      name="newPassword" 
                      value={inputs.newPassword || ""} 
                      onChange={handleChange}
                      placeholder='NewPassword...'
                      required
                    />
                    </label>
                    <input type="submit" />
          </form>
        </>
       )
      }}
     </ProfileContext.Consumer>
  )
}

export default Setting