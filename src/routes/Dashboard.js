import React, { Component } from 'react'
import './Dashboard.css'
import { Link } from 'react-router-dom'


export class Dashboard extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       todos: []
    }
  }
  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/todos')
    .then(res => res.json())
    .then(res => {this.setState({todos: res})})
  }
  render() {
    let {todos} = this.state
    if(todos.length>0){
      const firstTenTodos = todos.slice(0,10)
  return (
    <>
       <Link to="/setting">
       <div id='dashboardHeader'>
            <div className='dashboardHamburger'></div>
            <div  className='dashboardHamburger'></div>
            <div  className='dashboardHamburger'></div>
       </div>
       </Link>
       <table id='table'>
        <thead id='header'>
        <tr>
            <th>UserId</th>
            <th>Id</th>
            <th>Title</th>
            <th>Completed</th>
        </tr>
        </thead>
         
           <tbody>
          {firstTenTodos.map(todo => {
            return (
              <>
              <tr>
                  <td>{todo.userId}</td>
                  <td>{todo.id}</td>
                 <td> <a href={`todo/${todo.id}`}>{todo.title}</a></td>
                  <td>{String(todo.completed)}</td>
              </tr>
              </>
            )
          })}
          </tbody>
      </table>
      
    </>
  )
    }
    else
    return null
  }
}

export default Dashboard
