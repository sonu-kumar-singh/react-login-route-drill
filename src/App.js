import React from 'react';
import { useState } from 'react';
//import ReactDOM from 'react-dom/client';
import { useNavigate } from 'react-router-dom';
import './App.css'
import {ProfileContext} from './Contexts/ProfileContextComponent';



function MyForm() {
  const navigate = useNavigate();
  const [inputs, setInputs] = useState({});
  const [errorMessage, setErrorMessage] = useState("");
  
  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => 
      ({...values, [name]: value}))
  }
  
   

  return (
  
    <ProfileContext.Consumer>
    {(context) => {
      const { userName, password } = context
      const handleSubmit = (event) => {
        event.preventDefault();
        if(inputs.inputUsername === userName && inputs.inputPassword === password){
          navigate('/dashboard');
        }else{
          if(inputs.inputUsername !== userName && inputs.inputPassword !==  password){
            setErrorMessage("Username and Password is wrong")
          }
          else if(inputs.inputPassword !== password){
            setErrorMessage("Password is wrong") 
          }
          else{
            setErrorMessage("Username is wrong")
          }
        }
        
      }


    return (
      <>
      {errorMessage && <div className="error"> {errorMessage} </div>}
    <form onSubmit={handleSubmit}>
      <label>Username<br/>
      <input 
        type="text" 
        name="inputUsername" 
        value={inputs.inputUsername || ""} 
        onChange={handleChange}
        placeholder='Username...'
        required
      />
      </label>
      <label>Password<br/>
        <input 
          type="text" 
          name="inputPassword" 
          value={inputs.inputPassword || ""} 
          onChange={handleChange}
          placeholder='Password...'
          required
        />
        </label>
        <input type="submit" />
    </form>
    </>)
    }}
    </ProfileContext.Consumer>
  
  )
}


export default MyForm;

