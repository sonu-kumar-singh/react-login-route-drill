import React from "react";
import {createContext} from 'react'

export const ProfileContext= createContext()


class ProfileContextComponent extends React.Component{
   
    
      constructor(props) {
        super(props)
      
        this.state = {
            userName: 'admin',
            password: 'admin123'
         
        }
      }
    

    changeData = (newPassword) => {
      this.setState({ password: newPassword})
    }
    render(){
        return (
          <ProfileContext.Provider value={{...this.state, changeData:this.changeData}}>
            {this.props.children}
          </ProfileContext.Provider>
        )
    }
}

export default ProfileContextComponent