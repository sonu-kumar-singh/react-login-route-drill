import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Dashboard from './routes/Dashboard';
import ErrorPage from './routes/ErrorPage';
import Details from './routes/Details';
import Setting from './routes/Setting';
import reportWebVitals from './reportWebVitals';
import ProfileContextComponent from './Contexts/ProfileContextComponent';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
  },
  {
    path: "dashboard",
    element: <Dashboard />,
  },
  {
    path: "todo",
    element: <Details />,
    children: [
      {
        path: ":todoId",
        element: <Details />,
      },
    ],
  },
  {
    path: "setting",
    element: <Setting />
  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <ProfileContextComponent>
       <RouterProvider router={router} />
  </ProfileContextComponent>
   
  
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
